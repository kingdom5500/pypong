#!/usr/bin/env python3

import time
from tkinter import Tk, Frame, Canvas

from events import EventManager


class PongGame:

    WIDTH = 600
    HEIGHT = 500
    ASPECT_RATIO = WIDTH / HEIGHT

    FRAME_RATE = 60
    FRAME_TIME = 1 / FRAME_RATE

    def __init__(self):
        # Window/display initialisation
        self.root = Tk()
        self.root.title = "Pong!"

        self.scale = 0.0

        self.frame = Frame(self.root)
        self.frame.bind("<Configure>", self._aspect_ratio)
        self.frame.config(background="black")
        self.frame.pack(expand=True, fill="both")

        self.canvas = Canvas(self.frame)
        self.canvas.place()

        # Game initialisation
        self.event_manager = EventManager()
        self.event_manager.bind(self.canvas)

        self.is_running = False
        self.screen_stack = []

    def _aspect_ratio(self, event):
        """
        An event handler that forces the aspect ratio of the canvas
        within its frame, and aligns it to the center of the window.
        """

        # Use the largest side as the controlling dimension when
        # scaled with the aspect ratio. This is so that the canvas
        # actually fits properly within its frame.
        if event.width / PongGame.ASPECT_RATIO > event.height:
            height = event.height
            width = int(event.height * PongGame.ASPECT_RATIO)

            # Align the canvas to the center (for letterboxing)
            x = (event.width - width) // 2
            y = 0

        else:
            width = event.width
            height = int(event.width / PongGame.ASPECT_RATIO)

            x = 0
            y = (event.height - height) // 2

        self.scale = width / PongGame.WIDTH
        self.canvas.place(x=x, y=y, width=width, height=height)

    def _init_game(self):
        """
        Initialise the game using the recommended configuration, so
        that the game will execute as expected for a player.
        """

        self.is_running = True
        self.screen_stack = []

    def _game_loop(self):
        """
        Execute the game loop. The game must be initialised prior to
        calling this method, either by using the `PongGame._init_game`
        method or with a custom initialisation for debugging purposes.
        """

        frame = 0
        curr_sec = 0

        while self.is_running:
            frame_start = time.time()

            # Print the FPS every second.
            if frame_start > curr_sec + 1:
                curr_sec = frame_start
                print("ticks per second:", frame)
                frame = 0

            # Process and render
            self.root.update()
            self.root.update_idletasks()

            # Lock the FPS to the default amount
            frame_length = time.time() - frame_start
            if frame_length < PongGame.FRAME_TIME:
                time.sleep(PongGame.FRAME_TIME - frame_length)

            frame += 1

    def run(self):
        self._init_game()
        self._game_loop()


if __name__ == "__main__":
    game = PongGame()
    game.run()
