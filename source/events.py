#!/usr/bin/env python3

from abc import ABC, abstractmethod


class InputManager(ABC):
    """
    An abstract base class for binding input managers to windows.
    """

    @abstractmethod
    def bind(self, root):
        pass


class KeyboardManager(InputManager):
    def __init__(self):
        self.keys = {}
        super().__init__()

    def is_pressed(self, keysym: str) -> bool:
        return self.keys.get(keysym, False)

    # Event handling
    def _key_press(self, event):
        self.keys[event.keysym] = True

    def _key_release(self, event):
        self.keys[event.keysym] = False

    def bind(self, root):
        root.bind("<Any-KeyPress>", self._key_press)
        root.bind("<Any-KeyRelease>", self._key_release)


class MouseManager(InputManager):
    def __init__(self):
        self._x = 0
        self._y = 0
        self.buttons = {}

    def is_clicked(self, button: str) -> bool:
        return self.buttons.get(button, False)

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def position(self):
        return self._x, self._y

    # Event handling
    def _button_press(self, event):
        self.buttons[event.num] = True

    def _button_release(self, event):
        self.buttons[event.num] = False

    def _motion(self, event):
        self._x = event.x
        self._y = event.y

    def bind(self, root):
        root.bind("<Any-ButtonPress>", self._button_press)
        root.bind("<Any-ButtonRelease>", self._button_release)
        root.bind("<Motion>", self._motion)


class EventManager(InputManager):
    """
    A combination of the KeyboardManager and MouseManager.
    """

    def __init__(self):
        self.keyboard = KeyboardManager()
        self.mouse = MouseManager()

    def bind(self, root):
        self.keyboard.bind(root)
        self.mouse.bind(root)
